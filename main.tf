resource "aws_lambda_function" "aws_lambda_function-1e320c3e" {
  provider = aws.eu-central-1


  tags = {
    env = "development"
  }
}

resource "aws_cognito_user_pool" "GoZeit-cognito" {
  provider = aws.eu-central-1

  name = "GoZeit-cognito"

  tags = {
    env = "development"
  }
}

resource "aws_cloudfront_distribution" "GoZeit-cf" {
  provider = aws.eu-central-1


  tags = {
    env = "development"
  }
}

resource "aws_ses_email_identity" "aws_ses_email_identity-4b588fbd" {
  provider = aws.eu-central-1

}

resource "aws_route53_zone" "GoZeit-route53-zone" {
  provider = aws.eu-central-1

  name = "GoZeit-route53-zone"

  tags = {
    env = "development"
  }
}

resource "aws_cloudwatch_dashboard" "aws_cloudwatch_dashboard-646027d9" {
  provider = aws.eu-central-1

}

resource "aws_apigatewayv2_api" "GoZeit-api-gw" {
  provider = aws.eu-central-1

  name = "GoZeit-api-gw"

  tags = {
    env = "development"
  }
}

resource "aws_lambda_function" "aws_lambda_function-864c7820" {
  provider = aws.eu-central-1


  tags = {
    env = "development"
  }
}

resource "aws_s3_bucket" "aws_s3_bucket-9513ce4f" {
  provider = aws.eu-central-1


  tags = {
    env = "test"
  }
}

resource "aws_lambda_function" "aws_lambda_function-a66c58f9" {
  provider = aws.eu-central-1


  tags = {
    env = "development"
  }
}

resource "aws_s3_bucket" "aws_s3_bucket-ac362a72" {
  provider = aws.eu-central-1


  tags = {
    env = "test"
  }
}

resource "aws_dynamodb_global_table" "aws_dynamodb_global_table-cc2f0b8f" {
  provider = aws.eu-central-1

}

resource "aws_lambda_function" "aws_lambda_function-e94ef1e8" {
  provider = aws.eu-central-1


  tags = {
    env = "development"
  }
}